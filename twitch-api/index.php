<html>
    <head>
        <!-- intelijens - m.bui -->
        <title>Free Code Camp- Orange County, CA</title>

        <link rel="stylesheet" href="style.css">
        <link href="https://www.twitch.tv//favicon.ico" rel="shortcut icon" type="image/x-icon">
    </head>
    <body>
        <p>
            <img src="http://s.jtvnw.net/jtv_user_pictures/hosted_images/GlitchIcon_purple.png" />
        </p>
        <h1>Featured Twitch Streams</h1>
        <div id="unit"></div>
        <div id="outer">
            <ul id="inner">
            </ul>
        </div>

        <div id="links">
            <a href = "https://gitter.im/FreeCodeCamp/OrangeCounty" target="_blank"><img src="https://avatars-01.gitter.im/gh/u/gitterHQ?s=48" class="icons"/></a>
            <a href = "http://www.meetup.com/Free-Code-Camp-Orange-County-CA/" target="_blank"><img src="https://avatars-01.gitter.im/gh/u/FreeCodeCamp?s=48" class="icons"/></a>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
        <script src="api.js"></script>

    </body>
</html>
